﻿namespace ModManCore.Models
{
    public class ModManagerConfigFileModel
    {
        public int Version { get; set; }
        public string ConfigUrl { get; set; }

    }
}