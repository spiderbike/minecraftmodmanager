﻿using System;
using System.Collections.Generic;

namespace ModManCore.Models
{
    public class ServerFileConfigModel
    {
        public string ServerName { get; set; }
        public string ServerAddress { get; set; }
        public int ServerPort { get; set; }
        public DateTime ReleaseDate { get; set; }
        public List<FileDetails> ModFiles { get; set; } = new();

        public string UniqueTag => $"{ServerAddress}{ServerPort}{ServerName}";
    }

    public class FileDetails
    {
        public Uri FileUri { get; set; }
        public string Md5 { get; set; }
        public Uri InformationUri { get; set; }
    }
}
