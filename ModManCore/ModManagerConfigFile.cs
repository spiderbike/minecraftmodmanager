﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;
using ModManCore.Models;

namespace ModManCore
{
    public class ModManagerConfigFile
    {
        internal ModManagerConfigFileModel LoadModManagerConfig(string configPath)
        {
            // read file into a string and deserialize JSON to a type
            var jsonString = File.ReadAllText(configPath);
            var response = JsonSerializer.Deserialize<ModManagerConfigFileModel>(jsonString);
            return response;
        }
    }
}
