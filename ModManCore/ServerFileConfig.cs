﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using ModManCore.Models;

namespace ModManCore
{
    public class ServerFileConfig
    {
        public async Task<ServerFileConfigModel> LoadConfigFromUrl(string currentConfigUrl)
        {
            string errorText = null;
            try
            {
                using WebClient wc = new WebClient();
                var jsonString = await wc.DownloadStringTaskAsync(currentConfigUrl);

                return JsonSerializer.Deserialize<ServerFileConfigModel>(jsonString);
            }
            catch (WebException e)
            {
                errorText = $"There was a problem loading the data from the URL:\r\n\r\n{currentConfigUrl}\r\n{e.Message}\r\n\r\nDo you want to remove it?";
            }
            catch (JsonException e)
            {
                errorText = $"The config file was not valid:\r\n\r\n{currentConfigUrl}\r\n\r\nDo you want to remove it?";
            }
            catch (Exception e)
            {
                errorText = $"There was an error reading the config file from:\r\n\r\n{currentConfigUrl}\r\n{e.Message}\r\n\r\nDo you want to remove it?";
            }
            return null;
        }
    }
}
