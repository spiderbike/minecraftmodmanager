﻿using System;
using System.IO;
using System.Linq;
using System.Text.Json;
using ModManCore.Models;

namespace ModManCore
{
    public class Settings
    {
        public readonly string InternalSettingsPath;

        public Settings(string internalSettingsPath)
        {
            InternalSettingsPath = internalSettingsPath;
        }


        public SettingsModel LoadSettings()
        {
            if (!File.Exists(InternalSettingsPath))
            {
                return new SettingsModel();
            }

            var jsonString = File.ReadAllText(InternalSettingsPath);
            var masterConfig = JsonSerializer.Deserialize<SettingsModel>(jsonString);
            if (masterConfig == null)
            {
                return new SettingsModel();
            }
            return masterConfig;
        }


        public void AddUrl(string url)
        {
            var currentSettings = LoadSettings();
            if (currentSettings.ServerConfigUrls.Any(s =>
                s.Equals(url, StringComparison.InvariantCultureIgnoreCase)))
            {
                //ServerConfigUrl already in the settings, just return
                return;
            }
            currentSettings.ServerConfigUrls.Add(url);
            SaveSettings(currentSettings);
        }




        public void SaveSettings(SettingsModel config)
        {
            var options = new JsonSerializerOptions
            {
                WriteIndented = true
            };
            var jsonString = JsonSerializer.Serialize(config, options);
            File.WriteAllText(InternalSettingsPath, jsonString);
        }



    }
}
