﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Figgle;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using ModManCore;
using ModManUi;

namespace ModManConsole
{
    class MainService : IHostedService
    {
        private readonly IHostApplicationLifetime _appLifetime;
        private readonly Settings _settings;
        private readonly UiUtils _uiUtils;


        public MainService(IHostApplicationLifetime appLifetime, Settings settings, UiUtils uiUtils)
        {
            _appLifetime = appLifetime;
            _settings = settings;
            _uiUtils = uiUtils;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {

            await Console.Out.WriteLineAsync(FiggleFonts.Slant.Render("ModMan"));

            var settings = _settings.LoadSettings();
            var uiMainData = await _uiUtils.GetDataMainUiAsync(settings);


            _settings.AddUrl("http://www.google.com");
            settings = _settings.LoadSettings();


            if (Debugger.IsAttached)
            {
                await Console.Out.WriteLineAsync("press any key to exit.");
                Console.ReadKey();
            }
            _appLifetime.StopApplication();
        }


        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }

    class Program
    {
        private const string AppSettings = "appsettings.json";
        private const string HostSettings = "hostsettings.json";

        static async Task Main(string[] args)
        {
            var builder = new HostBuilder()
                .ConfigureHostConfiguration(configHost =>
                {
                    configHost.SetBasePath(Directory.GetCurrentDirectory());
                    configHost.AddJsonFile(HostSettings, optional: true);
                    configHost.AddCommandLine(args);
                })
                .ConfigureAppConfiguration((hostContext, configApp) =>
                {
                    configApp.SetBasePath(Directory.GetCurrentDirectory());
                    configApp.AddJsonFile(AppSettings, optional: true);
                    configApp.AddJsonFile($"appsettings.{hostContext.HostingEnvironment.EnvironmentName}.json", optional: true);
                    configApp.AddEnvironmentVariables(prefix: "ModMan_");
                    configApp.AddCommandLine(args);
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.Configure<ConsoleLifetimeOptions>(options =>
                    {
                        options.SuppressStatusMessages = true;
                    });

                    services.AddSingleton<IHostedService, MainService>();
                    services.Configure<ModSettings>(hostContext.Configuration.GetSection("settings"));

                    services.AddSingleton(sp =>
                    {
                        var modSettings = sp.GetRequiredService<IOptions<ModSettings>>();
                        return new Settings(modSettings.Value.InternalSettingsPath);
                    });

                    services.AddSingleton<ServerFileConfig>();
                    services.AddSingleton<UiUtils>();
                });

            await builder.RunConsoleAsync();
        }
    }

    internal class ModSettings
    {
        public string InternalSettingsPath { get; set; }
    }
}
