﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;
using Microsoft.Win32;
using MinecraftModManager.Models;
using Path = System.IO.Path;

namespace MinecraftModManager
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        private Configuration _configuration;
        private InternalConfig _masterConfig;

        public Settings()
        {
            InitializeComponent();
            _configuration = new Configuration();
        }


        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _masterConfig = _configuration.LoadMasterConfig();
            ClearErrors();
            UpdateUiFromModel();
            ValidateForm();
        }


        private void UpdateUiFromModel()
        {
            if (_masterConfig.CustomLauncherLocationPath != null)
            {
                CustomLauncherLocationCheckBox.IsChecked = true;
                CustomLauncherLocationPath.Text = _masterConfig.CustomLauncherLocationPath;
            }
        }


        private void WindowClosing(object sender, CancelEventArgs e)
        {
            var validationResults = GetValidationErrors();
            ShowErrorsInWindow(validationResults);

            //stop closing the window if errors present
            if (validationResults.Errors.Any())
            {
                MessageBoxResult result = MessageBox.Show("There are still issues, do you want to close anyway.", Defaults.AppTitle, MessageBoxButton.YesNo);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        break;
                    case MessageBoxResult.No:
                        e.Cancel = true;
                        break;
                }
            }

            //Code on the modal window
            var mainWindow = Owner as MainWindow;
            mainWindow.ReloadMasterConfig(); // Call your method here.
        }

        private void ShowErrorsInWindow(ValidationStatus validationResults)
        {
            ClearErrors();
            foreach (var error in validationResults.Errors)
            {
                switch (error.ValidationItem)
                {
                    case ValidationItem.CustomLauncherPath:
                        SetError(CustomLauncherLocationPathCell, error.Message);
                        break;
                        //default:
                        //    Console.WriteLine("Default case");
                        //    break;
                }

            }
        }

        private void ClearErrors()
        {

            foreach (WrapPanel tb in FindVisualChildren<WrapPanel>(MainGrid))
            {
                foreach (UIElement childItem in tb.Children)
                {
                    if (childItem.GetType().Name == "Image" || childItem.GetType().Name == "TextBlock")
                    {
                        childItem.Visibility = Visibility.Hidden;
                    }
                    else if (childItem.GetType().Name == "TextBox")
                    {
                        ((TextBox)childItem).Style = null;
                    }

                }
            }
        }


        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

        private void SetError(WrapPanel wrapPanel, string message)
        {
            foreach (UIElement childItem in wrapPanel.Children)
            {
                if (childItem.GetType().Name == "Image")
                {
                    childItem.Visibility = Visibility.Visible;
                }
                else if (childItem.GetType().Name == "TextBlock")
                {
                    childItem.Visibility = Visibility.Visible;
                    ((TextBlock)childItem).Text = message;
                }
                else if (childItem.GetType().Name == "TextBox")
                {
                    childItem.Visibility = Visibility.Visible;
                    ((TextBox)childItem).Style = (Style)Resources["ErrorTextBoxStyle"];
                }
            }
        }

        private ValidationStatus GetValidationErrors()
        {
            var validationStatus = new ValidationStatus();

            var launcherPathValidationResult = ValidateCustomLauncherPath();
            if (launcherPathValidationResult != null)
            {
                validationStatus.Errors.Add(launcherPathValidationResult);
            }
            return validationStatus;
        }

        private ValidationError ValidateCustomLauncherPath()
        {
            if (!CustomLauncherLocationPath.Text.EndsWith("MinecraftLauncher.exe", StringComparison.InvariantCultureIgnoreCase))
            {
                return new ValidationError
                {
                    ValidationItem = ValidationItem.CustomLauncherPath,
                    Message = "This path must point to MinecraftLauncher.exe"
                };
            }
            if (! File.Exists(CustomLauncherLocationPath.Text))
            {
                return new ValidationError
                {
                    ValidationItem = ValidationItem.CustomLauncherPath,
                    Message = "The file MinecraftLauncher.exe does not exist in that path, please check you are using the full path."
                };
            }


            return null;
        }

        private void ValidateFormFromUi(object sender, RoutedEventArgs e)
        {
            ValidateForm();
        }
        private void ValidateForm()
        {
            var validationResults = GetValidationErrors();
            ShowErrorsInWindow(validationResults);


            var validationErrors = GetValidationErrors();
            if (validationErrors.Errors.All(v => v.ValidationItem != ValidationItem.CustomLauncherPath))
            {
                _masterConfig.CustomLauncherLocationPath = CustomLauncherLocationPath.Text;
                _configuration.SaveMasterConfig(_masterConfig);
            }

        }

        private void CustomLauncherLocationBrowseButton_Click(object sender, RoutedEventArgs e)
        {
            //Create file dialog and configure to open csv files only
            string initialDir = "C:\\Program Files (x86)\\Minecraft Launcher";

            if (_masterConfig.CustomLauncherLocationPath != null)
            {
                if (File.Exists(initialDir))
                {
                    initialDir = _masterConfig.CustomLauncherLocationPath;
                }
            }
            OpenFileDialog launcherFilePicker = new OpenFileDialog {Filter = "MinecraftLauncher.exe|*.exe", CheckFileExists = true, InitialDirectory = initialDir};

            //Show the dialog to the user
            bool? result = launcherFilePicker.ShowDialog();

            //If the user selected something perform the actions with the file
            if (result.HasValue && result.Value)
            {
                var fileExists = File.Exists(launcherFilePicker.FileName);
                _masterConfig.CustomLauncherLocationPath = launcherFilePicker.FileName;
                _configuration.SaveMasterConfig(_masterConfig);
                CustomLauncherLocationPath.Text = launcherFilePicker.FileName;
                ValidateForm();
            }
        }
    }
    internal class ValidationStatus
    {
        internal List<ValidationError> Errors { get; set; } = new();

    }
    internal class ValidationError
    {
        internal ValidationItem? ValidationItem { get; set; }
        public string Message { get; set; }
    }

    internal enum ValidationItem
    {
        CustomLauncherPath
    }
}
