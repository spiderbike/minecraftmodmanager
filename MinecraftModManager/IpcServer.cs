﻿using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MinecraftModManager
{
    class IpcServer
    {





        void _ConnectionHandler(IAsyncResult result)
        {
            var srv = result.AsyncState as NamedPipeServerStream;
            srv.EndWaitForConnection(result);
            // we're connected, now deserialize the incoming command line
            var bf = new BinaryFormatter();
            var inargs = bf.Deserialize(srv) as string[];

            // process incoming command line
            _ProcessCommandLine(inargs);
            srv.Close();

            _masterConfig = _configuration.LoadMasterConfig();

            Task.Run(async () =>
            {
                MessageBox.Show("about UpdateDropdown()");
                await UpdateDropdown();

            });

            srv = new NamedPipeServerStream(Defaults.AppName + "IPC", PipeDirection.InOut, 1, PipeTransmissionMode.Message, PipeOptions.Asynchronous);
            srv.BeginWaitForConnection(new AsyncCallback(_ConnectionHandler), srv);
        }



        void _ProcessCommandLine(string[] args)
        {
            // we received some command line
            // arguments.
            // do actual work here

            foreach (var argument in args)
            {
                if (argument.EndsWith(_modManagerConfigExtension))
                {
                    _configuration.MoveConfigToConfigs(argument);
                }
            }
        }

    }
}
