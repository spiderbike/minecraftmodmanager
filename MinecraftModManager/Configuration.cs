﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using MinecraftModManager.Models;

namespace MinecraftModManager
{
    class Configuration
    {
        private readonly string _modManagerConfigFolder;
        private string _modManagerMainConfig;

        internal Configuration()
        {
            string tempMinecraftFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "MinecraftModManager");

            _modManagerConfigFolder = Path.Combine(tempMinecraftFolder, "configs");
            _modManagerMainConfig = Path.Combine(_modManagerConfigFolder, "main.config");

            if (!Directory.Exists(_modManagerConfigFolder))
            {
                Directory.CreateDirectory(_modManagerConfigFolder);
            }
        }


        internal InternalConfig LoadMasterConfig()
        {
            if (!File.Exists(_modManagerMainConfig))
            {
                return new InternalConfig();
            }

            var jsonString = File.ReadAllText(_modManagerMainConfig);
            var masterConfig = JsonSerializer.Deserialize<InternalConfig>(jsonString);
            if (masterConfig == null)
            {
                return new InternalConfig();
            }

            if (masterConfig.ServerConfigs.Any())
            {
                int serversBefore = masterConfig.ServerConfigs.Count;
                masterConfig.ServerConfigs = masterConfig.ServerConfigs.GroupBy(x => x.ServerUrl).Select(y => y.First())
                    .ToList();
                if (serversBefore > masterConfig.ServerConfigs.Count)
                {
                    //We removed some duplicate server configs, might as well save it
                    SaveMasterConfig(masterConfig);
                }
            }
            return masterConfig;
        }


        internal void SaveMasterConfig(InternalConfig config)
        {
            var options = new JsonSerializerOptions
            {
                WriteIndented = true
            };
            var jsonString = JsonSerializer.Serialize(config, options);
            File.WriteAllText(_modManagerMainConfig, jsonString);
        }


        internal void MoveConfigToConfigs(string filePathOfConfig)
        {
            var filename = new FileInfo(filePathOfConfig).Name;
            if (!ConfigExistsInMaster(filePathOfConfig))
            {
                AddConfigToMasterConfig(filePathOfConfig);
            }
        }

        internal void AddConfigToMasterConfig(string filePathOfConfig)
        {
            var masterConfig = LoadMasterConfig();
            var newConfig = LoadModManagerConfig(filePathOfConfig);
            masterConfig.ServerConfigs.Add(new ServerConfig
            {
                ServerUrl = newConfig.ConfigUrl
            });
            SaveMasterConfig(masterConfig);
        }

        internal bool ConfigExistsInMaster(string filePathOfConfig)
        {
            var masterConfig = LoadMasterConfig();
            var newConfig = LoadModManagerConfig(filePathOfConfig);

            if (masterConfig.ServerConfigs.Any(c => c.ServerUrl.Equals(newConfig.ConfigUrl)))
            {
                return true;
            }
            return false;
        }




        internal ModManagerConfig LoadModManagerConfig(string configPath)
        {
            // read file into a string and deserialize JSON to a type
            var jsonString = File.ReadAllText(configPath);
            var response = JsonSerializer.Deserialize<ModManagerConfig>(jsonString);


            return response;

        }



    }
}
