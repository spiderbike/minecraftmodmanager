﻿using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MinecraftModManager
{
    public static class Program
    {
        [System.STAThreadAttribute()]
        public static void Main()
        {
            // we need to get our app name so that we can create unique names for our mutex and our pipe

            var notAlreadyRunning = true;
            string[] args = Environment.GetCommandLineArgs();

            using (var mutex = new Mutex(true, Defaults.AppName, out notAlreadyRunning))
            {
                if (notAlreadyRunning)
                {
                    MinecraftModManager.App app = new MinecraftModManager.App();
                    app.InitializeComponent();
                    app.Run();

                    // do additional work here, startup stuff
                    Console.WriteLine("Running. Press any key to exit...");
                    // ...
                    // now process our initial main command line
                    _ProcessCommandLine(args);
                    // start the IPC sink.
                    var srv = new NamedPipeServerStream(_AppName + "IPC", PipeDirection.InOut, 1,
                        PipeTransmissionMode.Message, PipeOptions.Asynchronous);
                    // it's easier to use the AsyncCallback than it is to use Tasks here:
                    // this can't block, so some form of async is a must

                    srv.BeginWaitForConnection(new AsyncCallback(_ConnectionHandler), srv);


                    InitializeComponent();

                    //srv.Close();
                }
                else // another instance is running
                {
                    // connect to the main app
                    var cli = new NamedPipeClientStream(".", Defaults.AppName + "IPC", PipeDirection.InOut);
                    cli.Connect();
                    var bf = new BinaryFormatter();
                    // serialize and send the command line
                    bf.Serialize(cli, args);
                    cli.Close();
                    // and exit
                }
            }

        }

        
    }
}
