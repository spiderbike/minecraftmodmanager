﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Documents;

namespace MinecraftModManager
{
    public class ConfigManager
    {
        public List<Server> Servers { get; set; } = new();
    }

    public class Server
    {
        public string ServerName { get; set; }
        public string ServerAddress { get; set; }
        public int ServerPort { get; set; }
        public List<Release> Releases { get; set; } = new();
    }

    public class FileDetails
    {
        public Uri FileUri { get; set; }
        public string Md5 { get; set; }
        public Uri InformationUri { get; set; }
    }

    public class Release
    {
        public DateTime ReleaseDate { get; set; }
        public List<FileDetails> ModFiles { get; set; } = new();

    }
}