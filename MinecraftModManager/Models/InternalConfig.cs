﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftModManager.Models
{
    class InternalConfig
    {
        public int Version { get; set; } = 1;
        public List<ServerConfig> ServerConfigs { get; set; } = new();
        public string SelectedConfig { get; set; }
        public string CustomLauncherLocationPath { get; set; }
    }

    public class ServerConfig
    {
        public string ServerUrl { get; set; }
    }
}
