﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftModManager.Models
{
    public class ModManagerConfig
    {
        public int Version { get; set; }
        public string ConfigUrl { get; set; }
    }
}
