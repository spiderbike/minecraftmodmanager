﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using fNbt;
using MinecraftModManager.Models;
using Path = System.IO.Path;

namespace MinecraftModManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly string _cacheFolder;
        private readonly string _modFolder;
        private readonly string _modBackupFolder;
        private readonly string _minecraftFolder;
        private readonly string _modManagerExe;
        private readonly string _modManagerMainConfig;
        private InternalConfig _masterConfig;
        private Configuration _configuration;
        private const string _modManagerConfigExtension = ".minemodman";
        private const string _applicationName = "Minecraft Mod Manager";
        private Dictionary<string, ServerAndConfigUrl> AllServers = new Dictionary<string, ServerAndConfigUrl>();
        

        public MainWindow()
        {

            string tempMinecraftFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "MinecraftModManager");
            _minecraftFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), ".minecraft");
            _modFolder = Path.Combine(_minecraftFolder, "mods");

            _modManagerExe = Path.Combine(tempMinecraftFolder, "releases", "MinecraftModManager.exe");
            _cacheFolder = Path.Combine(tempMinecraftFolder, "cache");
            _modBackupFolder = Path.Combine(tempMinecraftFolder, "modsBackup");


            _configuration = new Configuration();

            _masterConfig = _configuration.LoadMasterConfig();
        }


        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //_mainConfig = await LoadConfig();


            await _CreateFileAssociation();

            await ModsRestore();
            await UpdateDropdown();
        }

        private async Task _CreateFileAssociation()
        {
            var fileAssociations = new List<FileAssociation>
            {
                new()
                {
                    ExecutableFilePath = _modManagerExe,
                    Extension = ".minemodman",
                    FileTypeDescription = "File extension for the Minecraft Mod Manager",
                    ProgId = "minemodmanfile"
                }
            };
            FileAssociations.EnsureAssociationsSet(fileAssociations);
        }

        private async Task<ConfigManagerWithUrl> LoadConfigFromUrl(string currentConfigUrl)
        {
            string errorText = null;
            try
            {
                var config = new ConfigManager();
                using (WebClient wc = new WebClient())
                {
                    var jsonString = wc.DownloadString(currentConfigUrl);
                    var configManager = JsonSerializer.Deserialize<ConfigManager>(jsonString);
                    return new ConfigManagerWithUrl
                    {
                        ConfigManager = configManager,
                        Url = currentConfigUrl
                    };

                }
            }
            catch (WebException e)
            {
                errorText = $"There was a problem loading the data from the URL:\r\n\r\n{currentConfigUrl}\r\n{e.Message}\r\n\r\nDo you want to remove it?";
            }
            catch (JsonException e)
            {
                errorText = $"The config file was not valid:\r\n\r\n{currentConfigUrl}\r\n\r\nDo you want to remove it?";
            }
            catch (Exception e)
            {
                errorText = $"There was an error reading the config file from:\r\n\r\n{currentConfigUrl}\r\n{e.Message}\r\n\r\nDo you want to remove it?";
            }

            if (!string.IsNullOrEmpty(errorText))
            {
                MessageBoxResult result = MessageBox.Show(errorText, _applicationName, MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    var itemsToRemove = _masterConfig.ServerConfigs.Where(s => s.ServerUrl.Equals(currentConfigUrl, StringComparison.InvariantCultureIgnoreCase));
                    foreach (var itemToRemove in itemsToRemove.ToList())
                    {
                        _masterConfig.ServerConfigs.Remove(itemToRemove);
                    }
                    _configuration.SaveMasterConfig(_masterConfig);
                }
            }

            return null;
        }

        private static string GetMD5HashFromFile(string fileName)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(fileName))
                {
                    return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", string.Empty);
                }
            }
        }



        private async Task UpdateDropdown()
        {
            MessageBox.Show("Running UpdateDropdown()");

            ServerFileListBox.Items.Clear();
            var tasks = new List<Task<ConfigManagerWithUrl>>();


            foreach (var serverConfig in _masterConfig.ServerConfigs.ToList())
            {
                tasks.Add(LoadConfigFromUrl(serverConfig.ServerUrl));
            }


            var completedTaks = await Task.WhenAll(tasks);
            foreach (var completedTask in completedTaks)
            {
                if (completedTask == null)
                {
                    continue;
                }

                foreach (var server in completedTask.ConfigManager.Servers)
                {
                    var uniqueTag = GenerateUniqueTag(server.ServerAddress, server.ServerPort, server.ServerName);

                    if (!AllServers.ContainsKey(uniqueTag))
                    {
                        AllServers.Add(uniqueTag, new ServerAndConfigUrl
                        {
                            Server = server,
                            ConfigUrl = completedTask.Url
                        });
                        var item = new ComboBoxItem
                        {
                            Tag = uniqueTag,
                            Content = server.ServerName
                        };
                        ServerFileListBox.Items.Add(item);
                    }

                }
            }

            if (ServerFileListBox.Items.Count < 1)
            {
                LaunchButton.IsEnabled = false;
                _masterConfig.SelectedConfig = null;
                _configuration.SaveMasterConfig(_masterConfig);
                return;

            }

            if (_masterConfig.SelectedConfig != null)
            {
                var comboBoxItem = ServerFileListBox.Items.OfType<ComboBoxItem>().FirstOrDefault(x => x.Tag.ToString().Equals(_masterConfig.SelectedConfig, StringComparison.InvariantCultureIgnoreCase));
                if (comboBoxItem == null)
                {
                    ServerFileListBox.SelectedIndex = 0;
                    LaunchButton.IsEnabled = true;
                }
                else
                {
                    ServerFileListBox.SelectedIndex = ServerFileListBox.Items.IndexOf(comboBoxItem);
                    LaunchButton.IsEnabled = true;
                }
            }
        }

        private string GenerateUniqueTag(string serverServerAddress, int serverServerPort, string serverServerName)
        {
            return $"{serverServerAddress}{serverServerPort}{serverServerName}";
        }


        private async void CloseApp(object sender, RoutedEventArgs e)
        {
            await ShutDown();
        }

        private void UIElement_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }




        private async void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var selectedServer = GetSelectedServer();
            if (selectedServer == null)
            {
                return;
            }
            try
            {
                await Task.Run(() => SetupFiles(selectedServer));
            }
            catch (Exception exception)
            {
                this.Dispatcher.Invoke(() =>
                {
                    ProgressBar.Value = 0;
                    statusTextLong.Text = exception.Message;
                    statusTextShort.Text = "Poop something broke";

                    MessageBox.Show($"{exception.Message}\r\n{exception.StackTrace}", "Poop something broke");
                });
            }
        }

        private Server GetSelectedServer()
        {
            string selectedServerTag = null;
            this.Dispatcher.Invoke(() =>
            {
                selectedServerTag = ((ComboBoxItem)ServerFileListBox.SelectedItem).Tag.ToString();
            });

            foreach (var serverConfig in AllServers)
            {
                if (selectedServerTag.Equals(GenerateUniqueTag(serverConfig.Value.Server.ServerAddress,
                    serverConfig.Value.Server.ServerPort, serverConfig.Value.Server.ServerName)))
                {
                    return serverConfig.Value.Server;
                }
            }

            return null;
        }

        private async Task SetupFiles(Server server)
        {
            string realMinecraftFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), ".minecraft");

            Directory.CreateDirectory(_cacheFolder);
            var release = server.Releases.OrderByDescending(s => s.ReleaseDate).First();

            //TODO: Should we handle a config with no releases?
            var percentEach = 100f / release.ModFiles.Count;

            using (var client = new WebClient())
            {
                this.Dispatcher.Invoke(() =>
                {
                    statusTextShort.Text = "Downloading Mods";
                });

                int fileNumber = 0;
                foreach (var modFile in release.ModFiles)
                {
                    fileNumber++;
                    var filename = modFile.FileUri.Segments.Last();
                    var filepath = Path.Combine(_cacheFolder, filename);

                    this.Dispatcher.Invoke(() =>
                    {
                        ProgressBar.Value = (int)(percentEach * fileNumber);
                        statusTextLong.Text = $"Checking {filename}";
                    });

                    var needsDownload = true;

                    if (File.Exists(filepath))
                    {
                        //Check MD5
                        if (!string.IsNullOrEmpty(modFile.Md5))
                        {
                            if (modFile.Md5.Equals(GetMD5HashFromFile(filepath),
                                StringComparison.InvariantCultureIgnoreCase))
                            {
                                needsDownload = false;
                            }
                            else
                            {
                                //The MD5 is wrong, delete it and it will be downloaded again.
                                File.Delete(filepath);
                            }
                        }
                    }

                    if (needsDownload)
                    {
                        //Download the file to cache
                        client.DownloadFile(modFile.FileUri, filepath);
                    }

                }
            }
            await ClearGuiStatus();


            await ModsBackup();
            await ModsEmptyFolder();
            await MoveModsToLive(server);

            var selectedServer = GetSelectedServer();
            var serverAddress = selectedServer.ServerAddress;
            if (selectedServer.ServerPort != null && selectedServer.ServerPort != 25565)
            {
                serverAddress = string.Format("{serverAddress}:{serverInfo.ServerPort}");
            }
            await AddServerToServersDat(selectedServer.ServerName, serverAddress);

            var minecraftLauncherLocations = Defaults.LauncherLocations();

            if (_masterConfig.CustomLauncherLocationPath != null)
            {
                //If we have a custom location set, just check there.
                minecraftLauncherLocations = new List<string> { _masterConfig.CustomLauncherLocationPath };
            }

            foreach (var launcherLocation in minecraftLauncherLocations)
            {
                if (File.Exists(launcherLocation))
                {
                    _masterConfig.CustomLauncherLocationPath = launcherLocation;
                    _configuration.SaveMasterConfig(_masterConfig);
                    Process.Start($"\"{launcherLocation}\"");
                    await WaitForMinecraftToStop();
                    break;
                }
            }

            throw new Exception($"Error, can't find the Minecraft Launcher, I've tried; \r\n\r\n{string.Join("\n", minecraftLauncherLocations)}\r\n\r\nTry setting one in the settings menu.");
        }

        private async Task AddServerToServersDat(string serverInfoServerName, string serverAddress)
        {

            var serverDatPath = Path.Combine(_minecraftFolder, "servers.dat");
            if (!File.Exists(serverDatPath))
            {
                return;
            }


            var myFile = new NbtFile();
            myFile.LoadFromFile(serverDatPath);
            var myCompoundTag = myFile.RootTag;
            var serverList = myCompoundTag.Tags.FirstOrDefault() as NbtList;


            var foundServer = false;


            foreach (var server in serverList)
            {
                var ip = server["ip"].StringValue;

                if (ip.Equals(serverAddress, StringComparison.InvariantCultureIgnoreCase))
                {
                    foundServer = true;
                }
            }

            if (!foundServer)
            {
                var compound = new NbtCompound
                {
                    new NbtString("name", serverInfoServerName), new NbtString("ip", serverAddress)
                };
                serverList.Add(compound);
            }

            myFile.SaveToFile(serverDatPath, NbtCompression.None);
        }

        private async Task WaitForMinecraftToStop()
        {
            await ClearGuiStatus();


            var procManager = new ProcessManager();
            bool hasBeenRunning = false;
            int? launcherProcId = null;
            int? minecraftProcId = null;

            while (true)
            {
                if (launcherProcId != null && minecraftProcId != null)
                {


                    var isRunning = await procManager.IsProcessRunningStill((int)minecraftProcId);
                    if (!isRunning)
                    {
                        await ShutDown();
                    }

                }
                else
                {
                    var procs = await procManager.GetForegroundProcessName();
                    if (procs.Minecraft != null)
                    {
                        hasBeenRunning = true;
                        minecraftProcId = procs.Minecraft;

                        this.Dispatcher.Invoke(() =>
                        {
                            statusTextLong.Text = $"Minecraft is running, switching to simple check";
                        });
                    }
                    else if (procs.Launcher != null)
                    {
                        hasBeenRunning = true;
                        launcherProcId = procs.Launcher;

                        this.Dispatcher.Invoke(() =>
                        {
                            statusTextLong.Text = $"Launcher is running";
                        });
                    }
                    else if (procs.Launcher != null && procs.Minecraft != null)
                    {
                        hasBeenRunning = true;
                        launcherProcId = procs.Launcher;
                        minecraftProcId = procs.Minecraft;
                        Debug.WriteLine("");
                    }

                    if (procs.Launcher == null && procs.Minecraft == null && hasBeenRunning)
                    {
                        //Nothing is running, wait another few seconds and if still the case shut down
                        this.Dispatcher.Invoke(() =>
                        {
                            statusTextLong.Text = $"Launcher closed, waiting to see if Minecraft launches";
                        });
                        int countdown = 30;
                        while (countdown > 0)
                        {
                            this.Dispatcher.Invoke(() =>
                            {
                                statusTextShort.Text = countdown.ToString();
                            });

                            countdown--;
                            procs = await procManager.GetForegroundProcessName();
                            if (procs.Minecraft != null)
                            {
                                hasBeenRunning = true;
                                minecraftProcId = procs.Minecraft;
                                await ClearGuiStatus();
                                this.Dispatcher.Invoke(() =>
                                {
                                    statusTextLong.Text = $"Minecraft is running, switching to simple check";
                                });
                                break;
                            }

                            await Task.Delay(1000);
                        }

                        if (procs.Launcher == null && procs.Minecraft == null)
                        {
                            await ShutDown();
                        }
                    }

                }

                await Task.Delay(1000);
            }
        }

        private async Task ClearGuiStatus()
        {
            this.Dispatcher.Invoke(() =>
            {
                ProgressBar.Value = 0;
                statusTextLong.Text = $"";
                statusTextShort.Text = "";
            });
        }


        private async Task ShutDown()
        {
            await ModsRestore();
            this.Dispatcher.Invoke(() =>
            {
                statusTextLong.Text = $"We're all done, closing down";
            });

            await Task.Delay(3000);

            this.Dispatcher.Invoke(() =>
            {
                Application.Current.Shutdown();
            });
        }


        private async Task ModsEmptyFolder()
        {
            await ClearGuiStatus();

            DirectoryInfo di = new DirectoryInfo(_modFolder);
            int NumberOfRetries = 30;
            int DelayOnRetry = 1000;

            foreach (FileInfo file in di.GetFiles().Where(f => f.FullName.EndsWith(".jar")))
            {


                for (int i = 1; i <= NumberOfRetries; ++i)
                {
                    try
                    {
                        file.Delete();
                        break; // When done we can break loop
                    }
                    catch (IOException e)
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            statusTextShort.Text = $"File locked, waiting {NumberOfRetries - i}";
                            statusTextLong.Text = file.Name;
                        });
                        Thread.Sleep(DelayOnRetry);
                    }
                }
            }
            await ClearGuiStatus();
        }

        private async Task MoveModsToLive(Server server)
        {
            await ClearGuiStatus();

            var release = server.Releases.OrderByDescending(s => s.ReleaseDate).First();


            var percentEach = 100f / release.ModFiles.Count;
            int fileNumber = 0;
            foreach (var file in release.ModFiles)
            {
                fileNumber++;
                var filename = file.FileUri.Segments.Last();
                this.Dispatcher.Invoke(() =>
                {
                    ProgressBar.Value = (int)(percentEach * fileNumber);
                    statusTextShort.Text = "Copy mod from cache";
                    statusTextLong.Text = filename;
                });

                File.Copy(Path.Combine(_cacheFolder, filename), Path.Combine(_modFolder, filename));
            }
            await ClearGuiStatus();

        }

        private async Task ModsBackup()
        {
            await ClearGuiStatus();

            this.Dispatcher.Invoke(() =>
            {
                ProgressBar.Value = 0;
                statusTextShort.Text = "Backup existing mods";
            });

            if (Directory.Exists(_modBackupFolder))
            {
                Directory.Delete(_modBackupFolder, true);
            }

            Directory.CreateDirectory(_modBackupFolder);
            DirectoryCopy(_modFolder, _modBackupFolder, true);
            await ClearGuiStatus();

        }
        private async Task ModsRestore()
        {
            await ClearGuiStatus();

            this.Dispatcher.Invoke(() =>
            {
                ProgressBar.Value = 0;
                statusTextShort.Text = "Restore existing mods";
            });

            if (Directory.Exists(_modBackupFolder))
            {
                await ModsEmptyFolder();
                DirectoryCopy(_modBackupFolder, _modFolder, true);
                Directory.Delete(_modBackupFolder, true);
            }
            await ClearGuiStatus();
        }


        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();

            // If the destination directory doesn't exist, create it.       
            Directory.CreateDirectory(destDirName);

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string tempPath = Path.Combine(destDirName, file.Name);
                file.CopyTo(tempPath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string tempPath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, tempPath, copySubDirs);
                }
            }
        }

        private void ServerListUpdated(object sender, SelectionChangedEventArgs e)
        {
            var comboBox = ((ComboBox)e.OriginalSource);
            var selectedItem = (ComboBoxItem)comboBox.SelectedItem;

            _masterConfig.SelectedConfig = selectedItem.Tag.ToString();
            _configuration.SaveMasterConfig(_masterConfig);
        }

        private void OpenConfig(object sender, RoutedEventArgs e)
        {
            _configuration.SaveMasterConfig(_masterConfig);
            Settings modalWindow = new Settings { Owner = this };
            modalWindow.ShowDialog();
        }

        internal void ReloadMasterConfig()
        {
            _masterConfig = _configuration.LoadMasterConfig();
        }
    }

    internal class ConfigManagerWithUrl
    {
        public ConfigManager ConfigManager { get; set; }
        public string Url { get; set; }
    }

    internal class ServerAndConfigUrl
    {
        public Server Server { get; set; }
        public string ConfigUrl { get; set; }
    }
}
