﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MinecraftModManager
{
    class ProcessManager
    {
        private string _minecraftRegex = "FML early loading progress|Minecraft.*?[0-9]+.[0-9]+.[0-9+].*";

        internal async Task<bool> IsProcessRunningStill(int procId)
        {
            try
            {
                var proc = Process.GetProcessById(procId);
                return Regex.IsMatch(proc.MainWindowTitle, _minecraftRegex, RegexOptions.IgnoreCase);
            }
            catch (Exception e)
            {
                return false;
            }
        }


        internal async Task<AppProcIds> GetForegroundProcessName()
        {
            try
            {
                Process[] processlist = Process.GetProcesses();
                var LauncherProc = processlist.FirstOrDefault(p => p.MainWindowTitle.Contains("Minecraft Launcher", StringComparison.InvariantCultureIgnoreCase));
                var minecraftProc = processlist.FirstOrDefault(p => Regex.IsMatch(p.MainWindowTitle, _minecraftRegex, RegexOptions.IgnoreCase));

                var launcher = LauncherProc?.Id;

                var minecraft = minecraftProc?.Id;
                Debug.WriteLine($"LauncherRunning: {launcher} | Minecraft: {minecraft} {minecraft != null}");

                return new AppProcIds
                {
                    Launcher = launcher,
                    Minecraft = minecraft,
                };
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

    }

    internal class AppProcIds
    {
        public int? Launcher { get; set; }
        public int? Minecraft { get; set; }
    }
}
