﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftModManager
{
    static class Defaults
    {
        private static string _minecraftFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), ".minecraft");
        public static string AppTitle = "Minecraft Mod Manager";
        public static string AppName = Assembly.GetEntryAssembly().GetName().Name;

        internal static List<string> LauncherLocations()
        {
            return new()
            {
                "C:\\Program Files (x86)\\Minecraft Launcher\\MinecraftLauncher.exe" ,
                Path.Combine(_minecraftFolder, "minecraft launcher\\Minecraft Launcher.exe"),
                Path.Combine(_minecraftFolder, "minecraft launcher\\MinecraftLauncher.exe"),
                Path.Combine(_minecraftFolder, "minecraftlauncher\\Minecraft Launcher.exe"),
                Path.Combine(_minecraftFolder, "minecraftlauncher\\MinecraftLauncher.exe")
            };
        } 
    }
}