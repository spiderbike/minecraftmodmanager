﻿using System.Linq;
using Autofac;
using Autofac.Core;
using Autofac.Core.Registration;
using Autofac.Core.Resolving.Pipeline;
using Caliburn.Micro;

namespace ModMan
{
    public class EventAggregationAutoSubscriptionModule : Module
    {
        protected override void AttachToComponentRegistration(IComponentRegistryBuilder componentRegistry, IComponentRegistration registration)
        {
            registration.PipelineBuilding += (sender, pipeline) =>
            {
                pipeline.Use(PipelinePhase.Activation, MiddlewareInsertionMode.EndOfPhase, (c, next) =>
                {
                    next(c);

                    if (c.Instance == null)
                        return;

                    var interfaces = c.Instance.GetType().GetInterfaces();
                    if (interfaces.Any(t =>
                        t.IsConstructedGenericType && t.GetGenericTypeDefinition() == typeof(IHandle<>)))
                    {
                        c.Resolve<IEventAggregator>().SubscribeOnPublishedThread(c.Instance);
                    }
                });
            };
        }
    }
}