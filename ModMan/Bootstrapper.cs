﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Autofac;
using Caliburn.Micro;

namespace ModMan
{
    public class Bootstrapper : AutofacBootstrapper
    {
        private readonly ILog _logger = LogManager.GetLog(typeof(Bootstrapper));

        static Bootstrapper()
        {
            LogManager.GetLog = type => new DebugLog(type);
        }

        public Bootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<ShellViewModel>();
        }

      
        /// <summary>
        /// Override to include your own Autofac configuration after the framework has finished its configuration, but 
        /// before the container is created.
        /// </summary>
        /// <param name="builder">The Autofac configuration builder.</param>
        protected override void ConfigureContainer(ContainerBuilder builder)
        {
            _logger.Info("Configuring Container.");
            builder.RegisterType<Configuration>()
                .SingleInstance();
        }

        protected override void ConfigureBootstrapper()
        {
            base.ConfigureBootstrapper();
            EnforceNamespaceConvention = false;
        }
    }
}
