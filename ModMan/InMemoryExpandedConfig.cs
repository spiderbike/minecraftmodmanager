﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModMan
{
    class InMemoryExpandedConfig
    {
        public Dictionary<string, ServersConfigWithConfigUrl> ServerConfigsExpanded { get; set; } = new();
    }
}
