﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace ModMan
{
    class ShellViewModel : Screen
    {
        private readonly Configuration _configuration;
        private readonly IWindowManager _windowManager;
        private readonly Func<SettingsViewModel> _settingsViewModelAccessor;

        public ShellViewModel(Configuration configuration, IWindowManager windowManager, Func<SettingsViewModel> settingsViewModelAccessor)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _windowManager = windowManager ?? throw new ArgumentNullException(nameof(windowManager));
            _settingsViewModelAccessor = settingsViewModelAccessor ?? throw new ArgumentNullException(nameof(settingsViewModelAccessor));
        }

        protected override async Task OnActivateAsync(CancellationToken cancellationToken)
        {
            await ReloadConfig();

            await base.OnActivateAsync(cancellationToken);
        }

        public async Task ReloadConfig()
        {
            var serverList = await _configuration.LoadServerList();

            Servers = new BindableCollection<Server>(serverList);

            if (serverList.Any() && SelectedServer == null)
            {
                SelectedServer = serverList.First();
            }
        }


        public BindableCollection<Server> Servers
        {
            get => _servers;
            set
            {
                _servers = value;
                NotifyOfPropertyChange();
            }
        }

        private Server _selectedServer;
        private BindableCollection<Server> _servers;

        public Server SelectedServer
        {
            get => _selectedServer;
            set
            {
                _selectedServer = value;
                NotifyOfPropertyChange();
            }
        }

        public Task Close()
        {
            return TryCloseAsync();
        }

        public async Task OpenSettingsWindow()
        {
            await _windowManager.ShowDialogAsync(_settingsViewModelAccessor());
            await ReloadConfig();
        }

        //public bool CanSayHello => !string.IsNullOrWhiteSpace(Name);

        //public void SayHello()
        //{
        //    MessageBox.Show(string.Format("Hello {0}!", Name)); //Don't do this in real life :)
        //}
    }
}
