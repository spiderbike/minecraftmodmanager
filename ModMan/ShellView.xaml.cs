﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ModMan
{
    /// <summary>
    /// Interaction logic for ShellView.xaml
    /// </summary>
    public partial class ShellView : Window
    {
        public ShellView()
        {
            InitializeComponent();
        }

        private void DragViaTopMenu(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
    }
}
