﻿using System.Collections.Generic;
using ModManCore.Models;

namespace ModManUi.Models
{
    public class UiMainModel
    {
        public List<ServerFileConfigModel> Servers = new();
        public string SelectedUrl { get; set; }
        public string CustomLauncherLocationPath { get; set; }
    }
}
