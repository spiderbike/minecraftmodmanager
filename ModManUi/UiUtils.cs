﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModManCore;
using ModManCore.Models;
using ModManUi.Models;

namespace ModManUi
{
    public class UiUtils
    {
        private readonly ServerFileConfig _serverFileConfig;

        public UiUtils(ServerFileConfig serverFileConfig)
        {
            _serverFileConfig = serverFileConfig;
        }


        public async Task<UiMainModel> GetDataMainUiAsync(SettingsModel settings)
        {
            var response = new UiMainModel
            {
                SelectedUrl = settings.SelectedConfig,
                CustomLauncherLocationPath = settings.CustomLauncherLocationPath
            };

            var tasks = new List<Task<ServerFileConfigModel>>();

            foreach (var serverConfigUrl in settings.ServerConfigUrls)
            {
                tasks.Add(_serverFileConfig.LoadConfigFromUrl(serverConfigUrl));
            }
            var allSdlServerDetails = await Task.WhenAll(tasks).ConfigureAwait(false);

            response.Servers = allSdlServerDetails.ToList();

            return response;
        }

    }
}
