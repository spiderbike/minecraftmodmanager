using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Text.RegularExpressions;

/// <summary>
/// An MSBuild task to create a list of preprocessor defines to be passed to candle from the
/// list of referenced projects.
/// </summary>
public sealed class CreateProjectDirHarvestDefineConstants : Task
{
    private ITaskItem[] defineConstants;
    private ITaskItem[] projectReferencePaths;

    [Output]
    public ITaskItem[] DefineConstants
    {
        get { return this.defineConstants; }
    }

    [Required]
    public ITaskItem[] ProjectReferencePaths
    {
        get { return this.projectReferencePaths; }
        set { this.projectReferencePaths = value; }
    }

    public override bool Execute()
    {
        List<ITaskItem> outputItems = new List<ITaskItem>();
        Dictionary<string, string> defineConstants = new Dictionary<string, string>();

        for (int i = 0; i < this.ProjectReferencePaths.Length; i++)
        {
            ITaskItem item = this.ProjectReferencePaths[i];

            string rootDir = item.GetMetadata("RootDir");
            string dir = item.GetMetadata("Directory");
            string subDir = item.GetMetadata("SubDir");
            

            string projectPath = CreateProjectDirHarvestDefineConstants.GetProjectPath(this.ProjectReferencePaths, i);
            string projectName = Path.GetFileNameWithoutExtension(projectPath);

            string referenceName = CreateProjectDirHarvestDefineConstants.GetReferenceName(item, projectName);

            string harvestOutputDirDefine = String.Format(CultureInfo.InvariantCulture, "{0}.HarvestOutputDir", referenceName);
            string harvestOutputDir = String.Format(CultureInfo.InvariantCulture, "{0}{1}{2}", rootDir, dir, subDir);

            defineConstants[harvestOutputDirDefine] = CreateProjectDirHarvestDefineConstants.EnsureEndsWithBackslash(harvestOutputDir);
        }

        foreach (KeyValuePair<string, string> define in defineConstants)
        {
            outputItems.Add(new TaskItem(String.Format(CultureInfo.InvariantCulture, "{0}={1}", define.Key, define.Value)));
        }
        this.defineConstants = outputItems.ToArray();

        return true;
    }

    public static string GetProjectPath(ITaskItem[] projectReferencePaths, int i)
    {
        return projectReferencePaths[i].GetMetadata("MSBuildSourceProjectFileFullPath");
    }

    private static readonly Regex AddPrefix = new Regex(@"^[^a-zA-Z_]", RegexOptions.Compiled);
    private static readonly Regex IllegalIdentifierCharacters = new Regex(@"[^A-Za-z0-9_\.]|\.{2,}", RegexOptions.Compiled); // non 'words' and assorted valid characters

    /// <summary>
    /// Return an identifier based on passed file/directory name
    /// </summary>
    /// <param name="name">File/directory name to generate identifer from</param>
    /// <returns>A version of the name that is a legal identifier.</returns>
    /// <remarks>This is duplicated from WiX's Common class.</remarks>
    internal static string GetIdentifierFromName(string name)
    {
        string result = IllegalIdentifierCharacters.Replace(name, "_"); // replace illegal characters with "_".

        // MSI identifiers must begin with an alphabetic character or an
        // underscore. Prefix all other values with an underscore.
        if (AddPrefix.IsMatch(name))
        {
            result = String.Concat("_", result);
        }

        return result;
    }

    public static string GetReferenceName(ITaskItem item, string projectName)
    {
        string referenceName = item.GetMetadata("Name");
        if (String.IsNullOrEmpty(referenceName))
        {
            referenceName = projectName;
        }

        referenceName = GetIdentifierFromName(referenceName);

        return referenceName;
    }

    private static string EnsureEndsWithBackslash(string dir)
    {
        if (dir[dir.Length - 1] != Path.DirectorySeparatorChar)
        {
            dir += Path.DirectorySeparatorChar;
        }

        return dir;
    }
}