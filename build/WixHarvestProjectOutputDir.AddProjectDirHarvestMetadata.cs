using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Text.RegularExpressions;

/// <summary>
/// An MSBuild task to create a list of preprocessor defines to be passed to candle from the
/// list of referenced projects.
/// </summary>
public sealed class AddProjectDirHarvestMetadata : Task
{
    [Output]
    public ITaskItem[] ProjectReferencePathsWithMetadata { get; set; }

    [Required]
    public ITaskItem[] ProjectReferencePaths { get; set; }
    
    public override bool Execute()
    {
        ProjectReferencePathsWithMetadata = ProjectReferencePaths;

        foreach (ITaskItem item in ProjectReferencePaths) {
            string projectPath = item.GetMetadata("MSBuildSourceProjectFileFullPath");
            string projectName = Path.GetFileNameWithoutExtension(projectPath);

            string referenceName = AddProjectDirHarvestMetadata.GetReferenceName(item, projectName);

            item.SetMetadata("DirectoryRefId", String.Format(CultureInfo.InvariantCulture, "{0}.ProjectDir", referenceName));
            item.SetMetadata("ComponentGroupName", String.Format(CultureInfo.InvariantCulture, "{0}.ProjectDir", referenceName));
            item.SetMetadata("PreprocessorVariable", String.Format(CultureInfo.InvariantCulture, "var.{0}.HarvestOutputDir", referenceName));
        }

        return true;
    }

    private static readonly Regex AddPrefix = new Regex(@"^[^a-zA-Z_]", RegexOptions.Compiled);
    private static readonly Regex IllegalIdentifierCharacters = new Regex(@"[^A-Za-z0-9_\.]|\.{2,}", RegexOptions.Compiled); // non 'words' and assorted valid characters

    /// <summary>
    /// Return an identifier based on passed file/directory name
    /// </summary>
    /// <param name="name">File/directory name to generate identifer from</param>
    /// <returns>A version of the name that is a legal identifier.</returns>
    /// <remarks>This is duplicated from WiX's Common class.</remarks>
    internal static string GetIdentifierFromName(string name)
    {
        string result = IllegalIdentifierCharacters.Replace(name, "_"); // replace illegal characters with "_".

        // MSI identifiers must begin with an alphabetic character or an
        // underscore. Prefix all other values with an underscore.
        if (AddPrefix.IsMatch(name))
        {
            result = String.Concat("_", result);
        }

        return result;
    }

    public static string GetReferenceName(ITaskItem item, string projectName)
    {
        string referenceName = item.GetMetadata("Name");
        if (String.IsNullOrEmpty(referenceName))
        {
            referenceName = projectName;
        }

        referenceName = GetIdentifierFromName(referenceName);

        return referenceName;
    }
}