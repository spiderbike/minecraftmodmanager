# How does it work?
Minecraft Mod Manager, is a tool that allows Minecraft server operators a way of automatically getting all the mods required to play on the server sent to the user via a launcher that runs prior to Minecraft.

The first thing you need to do is install the application, if you have not done so already, head over to [Installation Page](installation.md)

The Stages of operation are;

* When you first launch the application, the first thing it does is check for an update.
* Check for any updates
    * Install the latest version if there is an update
* Launch the main UI showing a drop down list of the defined servers

![Screenshot](img/main-ui.png)

```
If you don't have any servers in the drop list obtain the .mcfg file from the administrator of your Minecraft Server
```

When you click Go the following happens

* Check to see if we have each mod in our cache
* Any we do not have are downloaded to cache
* Wait for all the mods to be in cache
* Backup the mods currently in the mods folder
* Copy the mods required from cache to mods folder
* Automatic Launch of the Minecraft Launcher
* The app then waits for Minecraft to run
* If Minecraft is not detected as running after 30 seconds, or when Minecraft quits
    * Delete all the files from the mod folder
    * Restore the mods that were previously in the mods folder
    * Closes the APplication